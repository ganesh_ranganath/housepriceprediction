#HousePricePrediction

Disclaimer: The code and report were created as a part of final exam for UCSD's MATH 289C.

-----------------------------------------------------------------------------------------------------------------
Objective: To predict the sale price of houses using advance regression techniques. 

Competetion:https://www.kaggle.com/c/house-prices-advanced-regression-techniques

Dataset: Ames Housing Dataset

Error Metric: RMSE between log(Expected SalePrice) and log(Predicted SalePrice). 

-----------------------------------------------------------------------------------------------------------------
Language: R

Version: 3.2.3

-----------------------------------------------------------------------------------------------------------------
FILE DESCRIPTION:

1) data_description.txt: Data dictionary for the variables.

2) Data Table.pdf: Contains details about nature of variables (Categorical/Numerical) and list of variable which 
                  have an alternate definition for NA entries.
3) train.csv and test.csv: Comma Separated datafile containing training and test datset.                  
4) variable_change.txt: Contains a list of variable to be converted to factor and list of variables with alternate 
                       definition for NA.
5) dataProcess.Rmd : The data from train.csv and test.csv was cleaned and formatted.
6) EDA.Rmd: The cleaned training data was analysed and features were selected.
7) Regression.Rmd: Implementation of Linear Regression, Generalised Boostin Regression, LASSO, and Random Forest.
8) sample_submission.csv: Sample format for submission of prediction.
9) Report.pdf: Copy of Final Report
10) submission.png: Kaggle Submission Screenshot

-----------------------------------------------------------------------------------------------------------------
NOTE:

1) Execute the files in following order - dataProcess.Rmd, EDA.Rmd, and Regression.Rmd



